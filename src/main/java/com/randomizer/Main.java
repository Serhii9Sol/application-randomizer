package main.java.com.randomizer;

import main.java.com.randomizer.model.Model;
import main.java.com.randomizer.services.CommandService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Model model = new Model();
        CommandService commandService = new CommandService(reader, model);
        model.setCommandService(commandService);
        model.run();
    }
}
