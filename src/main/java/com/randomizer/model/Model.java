package main.java.com.randomizer.model;

import main.java.com.randomizer.services.CommandService;

public class Model {
    private int min;
    private int max;
    private int lengthOfDiapason;
    private boolean[] historyOfNumber;
    private CommandService commandService;

    public void setCommandService(CommandService commandService) {
        this.commandService = commandService;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getLengthOfDiapason() {
        return lengthOfDiapason;
    }

    public void setLengthOfDiapason(int lengthOfDiapason) {
        this.lengthOfDiapason = lengthOfDiapason;
    }

    public boolean[] getHistoryOfNumber() {
        return historyOfNumber;
    }

    public void setHistoryOfNumber(boolean[] historyOfNumber) {
        this.historyOfNumber = historyOfNumber;
    }

    public void run(){
        //Задаєм стартові умови
        do{} while(!commandService.setParametersOfModel(this));

        //Постійно запитуємо нову команду
        while(true){
            commandService.processCommand(commandService.getCommandFromConsole());
        }
    }
}
