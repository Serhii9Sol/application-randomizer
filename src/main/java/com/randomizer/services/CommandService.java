package main.java.com.randomizer.services;

import main.java.com.randomizer.model.Model;

import java.io.BufferedReader;
import java.io.IOException;

public class CommandService {
    private static final String CMD_GENERATE = "generate";
    private static final String CMD_HELP = "help";
    private static final String CMD_RESTART = "restart";
    private static final String CMD_EXIT = "exit";
    private BufferedReader reader;
    private Model model;


    public CommandService(BufferedReader reader, Model model) {
        this.reader = reader;
        this.model = model;
    }

    public boolean setParametersOfModel(Model model){
        try {
            System.out.println("Pls, write minimum number > 0");
            model.setMin(Integer.parseInt(reader.readLine()));
            System.out.println("Pls, write maximum number < 501");
            model.setMax(Integer.parseInt(reader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e){
            System.out.println("Invalid input. You add not number\n");
            return false;
        }
        model.setLengthOfDiapason(model.getMax() - model.getMin() +1 );
        if(model.getMin() < 1 || model.getMax() > 500 || model.getLengthOfDiapason() < 2){
            System.out.println("Invalid input\n");
            return false;
        }
        model.setHistoryOfNumber(new boolean[model.getLengthOfDiapason()]);
        return true;
    }

    public String getCommandFromConsole(){
        System.out.println("*********************************\n" +
                "Plz enter the command\n" +
                "generate - if you want to get random number between [" + model.getMin() + " - " + model.getMax() + "]\n" +
                "help - if you need help\n" +
                "restart - if you want to write new diapason of numbers\n" +
                "exit - if you want to end\n" + "*********************************");

        String nextCommand = null;
        try {
            nextCommand = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nextCommand;
    }

    public void processCommand(String command){
        switch(command){
            case CMD_GENERATE:
                if(hasVacantNumber(model)){
                    System.out.println(generateRandomNumber(model));
                } else {
                    System.out.println("All numbers in diapason [" + model.getMin() + " - " + model.getMax() +"] are generated");
                }
                break;
            case CMD_HELP:
                System.out.println("there should be help command realization))) ");
                break;
            case CMD_RESTART:
                model.run();
                break;
            case CMD_EXIT:
                System.out.println("End of program!");
                System.exit(0);
            default:
                System.out.println("Invalid command! Plz try again");
                break;
        }
    }

    public int generateRandomNumber(Model model){
        int randomNumber;
        do {
            randomNumber = (int) (Math.random() * model.getLengthOfDiapason() + model.getMin());
        } while(isAlreadyGenerated(randomNumber, model));
        return randomNumber;
    }

    private boolean isAlreadyGenerated(int number, Model model){
        if(model.getHistoryOfNumber()[number - model.getMin()]){
            return true;
        } else {
            model.getHistoryOfNumber()[number - model.getMin()] = true;
            return false;
        }
    }

    private boolean hasVacantNumber(Model model){
        for(int i = 0; i < model.getHistoryOfNumber().length; i++){
            if(!model.getHistoryOfNumber()[i]){
                return true;
            }
        }
        return false;
    }
}
