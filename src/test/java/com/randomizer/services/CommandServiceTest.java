package test.java.com.randomizer.services;

import main.java.com.randomizer.model.Model;
import main.java.com.randomizer.services.CommandService;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class CommandServiceTest {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final Model model = new Model();
    private final CommandService cut = new CommandService(reader,model);

    @Test
    void generateRandomNumberTest(){
        int min = 1;
        int max = 10;
        int lengthOfDiapason = max - min + 1;
        boolean[] historyOfNumber = new boolean[lengthOfDiapason];
        model.setHistoryOfNumber(historyOfNumber);
        model.setMax(max);
        model.setMin(min);
        model.setLengthOfDiapason(lengthOfDiapason);

        int[] actualSequenceOfRandomNumber = new int[lengthOfDiapason];
        for(int i = 0; i < lengthOfDiapason; i++){
            actualSequenceOfRandomNumber [i] = cut.generateRandomNumber(model);
        }
        Arrays.sort(actualSequenceOfRandomNumber);

//        boolean actual = true;
//        //если хоть один елемент повторится, то actual = false
//        for(int i = 0; i < lengthOfDiapason - 1; i++){
//            if(actualSequenceOfRandomNumber [i] == actualSequenceOfRandomNumber [i + 1]){
//                actual = false;
//            }
//        }
//        assertTrue(actual);
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertArrayEquals(expected, actualSequenceOfRandomNumber);
    }

}